# Introduction #
# dragonruby-spacegame

Just playing about with [Dragon Ruby](https://dragonruby.itch.io/)

## Running the project

The dragonruby executable files are needed - see [Dragon Ruby](https://dragonruby.itch.io/)  
The file [VERSION.txt] from the dragonruby distribution is included in this repository so that the build of Dragon Ruby used is tracked.  
When the executables are placed in this folder, `dragonruby` will run the game from source and `dragonruby-build` will build it.