require 'app/sprite_animation.rb'

class Explosion

  attr_reader :x, :y, :size, :angle, :sprite_animation, :current_animation_frame

  def initialize(x, y, size = 128)
    @x = x
    @y = y
    @size = size
    @angle = rand(360)
    @sprite_animation = @@sprite_animation_explosion
    @current_animation_tick = 0
    @current_animation_frame = 0
    @finished = false
  end

  def update
    @angle += 1
    @current_animation_tick += 1
    if @current_animation_tick >= @sprite_animation.frame_duration
      @current_animation_tick = 0
      @current_animation_frame += 1
      if @current_animation_frame >= @sprite_animation.frames.length
        if @sprite_animation.loops
          @current_animation_frame = 0
        else
          @current_animation_frame = @sprite_animation.frames.length - 1
          @finished = true
        end
      end
    end
  end

  def finished?
    @finished
  end

  def serialize
    {}
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end
end