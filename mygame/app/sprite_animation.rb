SpriteAnimationFrame = Struct.new(:top_left_x, :top_left_y)

# Defines a reusable sprite animation definition. Does not store the current state of an instance of an animation.
class SpriteAnimation

  attr_reader :frames, :sprite_sheet, :w, :h, :frame_duration, :loops

  def initialize(sprite_sheet, w, h, frame_duration, loops = true)
    @frames = []
    @sprite_sheet = sprite_sheet
    @w = w
    @h = h
    @frame_duration = frame_duration
    @loops = loops
  end

  def add_frame(x, y)
    @frames << SpriteAnimationFrame.new(x,y)
  end
end

@@sprite_animation_enemy_bug = SpriteAnimation.new("sprites/enemies.png", 32, 32, 4)
4.times { |i| @@sprite_animation_enemy_bug.add_frame(32 * i, 0) }

@@sprite_animation_enemy_wasp = SpriteAnimation.new("sprites/enemies.png", 32, 32, 1)
4.times { |i| @@sprite_animation_enemy_wasp.add_frame(32 * i, 32) }

@@sprite_animation_enemy_mine = SpriteAnimation.new("sprites/enemies.png", 32, 32, 2)
8.times { |i| @@sprite_animation_enemy_mine.add_frame(32 * i, 64) }

@@sprite_animation_enemy_blaster = SpriteAnimation.new("sprites/enemies.png", 32, 32, 2)
4.times { |i| @@sprite_animation_enemy_blaster.add_frame(32 * i, 96) }

@@sprite_animation_enemy_amoeba = SpriteAnimation.new("sprites/enemies.png", 32, 32, 2)
8.times { |i| @@sprite_animation_enemy_amoeba.add_frame(32 * i, 128) }

@@sprite_animation_enemy_cocoon = SpriteAnimation.new("sprites/enemies.png", 32, 32, 2)
4.times { |i| @@sprite_animation_enemy_cocoon.add_frame(32 * i, 160) }

@@sprite_animation_enemy_heavy = SpriteAnimation.new("sprites/enemies.png", 32, 32, 2)
4.times { |i| @@sprite_animation_enemy_heavy.add_frame(32 * i, 192) }

@@sprite_animation_heavy_missile = SpriteAnimation.new("sprites/enemies.png", 32, 32, 2)
3.times { |i| @@sprite_animation_heavy_missile.add_frame(32 * i, 224) }

@@sprite_animation_explosion = SpriteAnimation.new("sprites/explosion.png", 128, 128, 2, false)
@@sprite_animation_explosion.add_frame(0,0)
@@sprite_animation_explosion.add_frame(128,0)
@@sprite_animation_explosion.add_frame(128,0)
@@sprite_animation_explosion.add_frame(256,0)
@@sprite_animation_explosion.add_frame(256,0)
@@sprite_animation_explosion.add_frame(388,0)
@@sprite_animation_explosion.add_frame(388,0)
@@sprite_animation_explosion.add_frame(512,0)
@@sprite_animation_explosion.add_frame(512,0)
@@sprite_animation_explosion.add_frame(640,0)
@@sprite_animation_explosion.add_frame(640,0)
@@sprite_animation_explosion.add_frame(640,0)
@@sprite_animation_explosion.add_frame(768,0)
@@sprite_animation_explosion.add_frame(768,0)
@@sprite_animation_explosion.add_frame(768,0)
@@sprite_animation_explosion.add_frame(896,0)
@@sprite_animation_explosion.add_frame(896,0)
