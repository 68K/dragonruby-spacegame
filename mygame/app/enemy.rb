require 'app/sprite_animation.rb'
require 'app/heavy_missile.rb'

class Enemy

  attr_reader :type, :x, :y, :alive, :angle, :sprite_animation, :current_animation_frame, :speed, :health, :points

  def initialize(type, x, y)
    @type = type
    @x = x.to_f
    @y = y.to_f
    @alive = true
    @angle = 0.0
    @points = 10

    case @type
    when :bug
      @sprite_animation = @@sprite_animation_enemy_bug
      @speed = 4
      @health = 8
    when :wasp
      @sprite_animation = @@sprite_animation_enemy_wasp
      @speed = 7
      @health = 3
      @points = 20
    when :mine
      @sprite_animation = @@sprite_animation_enemy_mine
      @speed = 1
      @health = 10
    when :blaster
      @sprite_animation = @@sprite_animation_enemy_blaster
      @speed = 3
      @health = 20
      @current_fire_tick = 0
      @fire_cooldown = 50
      @points = 25
    when :amoeba
      @sprite_animation = @@sprite_animation_enemy_amoeba
      @speed = 4
      @health = 4
    when :cocoon 
      @sprite_animation = @@sprite_animation_enemy_cocoon
      @speed = 2
      @health = 4
    when :heavy
      @sprite_animation = @@sprite_animation_enemy_heavy
      @speed = 2
      @health = 15
      @points = 35
      @current_fire_tick = 0
      @fire_cooldown = 100
    else
      @sprite_animation = @@sprite_animation_enemy_bug
      @speed = 4
      @health = 1
    end

    @current_animation_tick = 0
    @current_animation_frame = 0
  end

  def update args
    case @type
    when :bug, :mine
      @y -= @speed
      @x -= 1 if @x > args.state.current_player_x
      @x += 1 if @x < args.state.current_player_x
    when :wasp
      if @y > 100 # Stop homing and just fall off the screen
        angle_to_player = Math.atan2(args.state.current_player_y - @y, args.state.current_player_x - @x) * 180 / Math::PI + 90
        @angle -= 3 if @angle - angle_to_player > 4.0
        @angle += 3 if @angle - angle_to_player < -4.0
      end
      @x += @speed * Math.sin(@angle * Math::PI / 180)
      @y -= @speed * Math.cos(@angle * Math::PI / 180)
    when :blaster
      @current_fire_tick -= 1 if @current_fire_tick > 0
      if @y > 600
        @y -= @speed
      else
        @x -= 1 if @x > args.state.current_player_x
        @x += 1 if @x < args.state.current_player_x
        if @current_fire_tick == 0
          @current_fire_tick = @fire_cooldown
          args.state.blasters_fired_this_tick = true
          args.state.blaster_bullets << args.state.new_entity(:blaster_bullet) do |b|
            b.x = @x
            b.y = @y - 20
            b.speed = 2
          end
        end
      end
    when :heavy
      @current_fire_tick -= 1 if @current_fire_tick > 0
      @y -= @speed
      # Do not fire from too low on the screen, the missiles move near-horizontal and may be unavoidable
      if @current_fire_tick == 0 && @y > 100
        @current_fire_tick = @fire_cooldown
        args.state.heavy_missiles << HeavyMissile.new(@x, @y + 8, 180)
      end
    else
      @y -= @speed
    end

    @current_animation_tick += 1
    if @current_animation_tick >= @sprite_animation.frame_duration
      @current_animation_tick = 0
      @current_animation_frame += 1
      if @current_animation_frame >= @sprite_animation.frames.length
        @current_animation_frame = @sprite_animation.loops ? 0 : @sprite_animation.frames.length - 1
      end
    end
  end

  # Apply damage
  # @return true if this damage caused death
  def apply_damage damage
    return false unless @alive
    @health = [@health - damage, 0].max
    @alive = false if @health <= 0
    !@alive
  end

  def collision_rect
    { x: @x - (@sprite_animation.w / 2), y: @y + (@sprite_animation.h / 2), w: @sprite_animation.w, h: @sprite_animation.h }
  end

  def serialize
    {}
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end
end