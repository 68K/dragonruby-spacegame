require 'app/enemy.rb'
require 'app/explosion.rb'
require 'app/sprite_animation.rb'

PlayerBullet = Struct.new(:x, :y, :speed, :spent)

def defaults args
  args.state.number_of_stars = 150
  init_starfield args unless args.state.stars

  args.state.current_state ||= :title_screen
  args.state.title_marquee_text ||= "                                                       " + 
                                    "  WELCOME TO MEGA SPACE GAME!!!!     DEVELOPED IN DRAGONRUBY BY JAMES STOCKS         " +
                                    "PRESS START OR SPACE TO BEGIN!       CREDITS TO... ALPHA CHROME YAYO @ ALPHACHROMEYAYO.BANDCAMP.COM " +
                                    "FOR IN-GAME MUSIC \"TAKE MY ADVICE\" AND GAME OVER JINGLES " +
                                    "       V-KTOR & JOBRO @ FREESOUND.ORG FOR SOUND EFFECTS      " +
                                    "SHOUTS TO... EVERYONE AT DISCORD.DRAGONRUBY.ORG        ALPHA CHROME YAYO       DA S.P.E.C.I.A.L FRIENDS      " +
                                    "HIT ME UP AT.... JSTOCKS@GMAIL.COM      GITLAB.COM/68K " +
                                    "                                                  "
  args.state.title_marquee_left_char ||= 0
  args.state.title_marquee_right_char ||= 1
  args.state.title_marquee_char_width ||= 20

  args.state.current_stage ||= 0

  args.state.hi_score ||= 1000

  args.state.player_speed ||= 6
  args.state.initial_player_x ||= 640.0
  args.state.initial_player_y ||= 32.0
  args.state.current_player_x ||= 640.0
  args.state.current_player_y ||= 32.0
  args.state.player_score ||= 0
  args.state.player_lives ||= 3
  args.state.player_currently_alive = true unless args.state.as_hash.keys.include?(:player_currently_alive)
  args.state.player_respawn_duration ||= 100
  args.state.player_respawn_current ||= 0
  args.state.player_iframes_duration ||= 150
  args.state.player_iframes_counter ||= 50

  # Player bullets
  args.state.player_bullets ||= []
  args.state.player_bullet_speed ||= 12
  args.state.player_bullet_damage ||= 2
  # Cooldown between bullets
  args.state.player_fire_cooldown_duration ||= 4
  args.state.player_fire_cooldown_current ||= 0
  # Player can fire continuously up to a certain volley size
  args.state.player_bullet_volley_size ||= 3
  args.state.player_current_bullet_volley_rounds ||= 3
  args.state.player_bullet_volley_cooldown_duration ||= 20
  args.state.player_bullet_volley_cooldown_current ||= 0

  # Enemies
  args.state.enemies ||= []
  args.state.next_enemy_ticker ||= 0
  args.state.enemy_spawn_period ||= 275

  args.state.blaster_bullets ||= []
  args.state.heavy_missiles ||= []

  # Explosions
  args.state.explosions ||= []
  # Mine explosions appear as a standard explosion but have a separate entity for processing their resulting damage
  args.state.mine_explosions ||= []

  # CD Access animation
  args.state.cd_animation_tick ||= 0
  args.state.cd_animation_max ||= 500
  args.state.cd_artist_name ||= "ALPHA CHROME YAYO"
  args.state.cd_track_name ||= "TAKE MY ADVICE"
end

def render args
  case args.state.current_state
  when :title_screen
    tiles = []
    args.state.title_tiles.length.times do |i|
      args.state.title_tiles[i].length.times do |j|
        tiles << { x: i * 72 - 72, y: j * 72 - 72, w: 72, h: 72, path: :pixel, r: args.state.title_tiles[i][j], g: args.state.title_tiles[i][j], b: 40 }
      end
    end
    args.outputs.sprites << tiles
    args.outputs.labels << [ 640, 500, 'MEGA SPACE GAME', 16, 1, 255, 255, 255 ]
    # TODO: Fix marquee
    #args.outputs.labels << [ 1300 - args.state.title_marquee_right_char * 15 - (args.tick_count % 4), 30, args.state.title_marquee_text[args.state.title_marquee_left_char...args.state.title_marquee_right_char], 2, 0, 255, 255, 255 ]
    args.outputs.sprites << [ 576, 210, 128, 128, 'sprites/player_ship.png' ]
  when :playing
    args.outputs.solids << [0, 0, 1280, 720, 0, 0, 0]
    args.outputs.solids << args.state.stars.map do |star|
        {
            x: star.x,
            y: star.y, 
            w: 4,
            h: 4, 
            r: star.sparkly ? rand(255) : 255,
            g: star.sparkly ? rand(255) : 255,
            b: star.sparkly ? rand(255) : 255,
            a: 80 * (args.state.tick_count % star.twinkle)
        }
    end

    # Only draw player if alive, flash player if in iframes
    if args.state.player_currently_alive && (!player_in_iframes?(args) || args.state.tick_count % 4 == 0)
      args.outputs.sprites << [ args.state.current_player_x - 32, args.state.current_player_y - 32, 64, 64, 'sprites/player_ship.png' ]
    end

    # CD animation
    if args.state.cd_animation_tick < args.state.cd_animation_max
      cd_alpha = [args.state.cd_animation_tick * 1.5, 256].min
      args.outputs.sprites << { x: 32, y: 50, w: 64, h: 64, angle: 3 * (args.state.tick_count % 360), path: 'sprites/CD.png', a: cd_alpha }
      artist_chars = [(args.state.cd_animation_tick / 10) - 10, 0].max
      track_chars = [(args.state.cd_animation_tick / 10) - 20, 0].max
      if artist_chars > 0
        args.outputs.labels << [ 330, 110, args.state.cd_artist_name[0...artist_chars], 4, 2, 100, 255, 100 ]
      end
      if track_chars > 0
        args.outputs.labels << [ 330, 85, args.state.cd_track_name[0...track_chars], 4, 2, 100, 255, 100 ]
      end
    end

    sprites_to_draw = [] 
    sprites_to_draw << args.state.player_bullets.map do |b|
      { x: b.x - 16, y: b.y - 16, w: 32, h: 32, path: 'sprites/player_bullet.png' }
    end

    sprites_to_draw << args.state.blaster_bullets.map do |b|
        { x: b.x - 16, y: b.y - 16, w: 32, h: 32, path: 'sprites/blaster_bullet.png' }
    end

    sprites_to_draw << args.state.heavy_missiles.map do |m|
        {
            x: m.x - (m.sprite_animation.w / 2),
            y: m.y - (m.sprite_animation.h / 2),
            w: m.sprite_animation.w,
            h: m.sprite_animation.h,
            angle: m.angle,
            path: 'sprites/enemies.png',
            tile_x: m.sprite_animation.frames[m.current_animation_frame].top_left_x,
            tile_y: m.sprite_animation.frames[m.current_animation_frame].top_left_y,
            tile_w: m.sprite_animation.w,
            tile_h: m.sprite_animation.h
        }
    end

    sprites_to_draw << args.state.enemies.map do |e|
        {
            x: e.x - (e.sprite_animation.w / 2),
            y: e.y - (e.sprite_animation.h / 2),
            w: e.sprite_animation.w,
            h: e.sprite_animation.h,
            angle: e.angle,
            path: 'sprites/enemies.png',
            tile_x: e.sprite_animation.frames[e.current_animation_frame].top_left_x,
            tile_y: e.sprite_animation.frames[e.current_animation_frame].top_left_y,
            tile_w: e.sprite_animation.w,
            tile_h: e.sprite_animation.h,
            flip_horizontally: false 
        } 
    end

    sprites_to_draw << args.state.explosions.map do |ex|
        {
            x: ex.x - (ex.size / 2),
            y: ex.y - (ex.size / 2),
            w: ex.size,
            h: ex.size,
            path: 'sprites/explosion.png',
            tile_x: ex.sprite_animation.frames[ex.current_animation_frame].top_left_x,
            tile_y: ex.sprite_animation.frames[ex.current_animation_frame].top_left_y,
            tile_w: ex.sprite_animation.w,
            tile_h: ex.sprite_animation.h,
            a: 240,
            angle: ex.angle 
        }
    end

    args.outputs.sprites << sprites_to_draw

    args.outputs.labels << [ 1050, 680, "SCORE: #{args.state.player_score}", 3, 0, 255, 255, 255 ]
    args.state.player_lives.times { |i| args.outputs.sprites << [1050 + i * 20, 635, 20, 20, 'sprites/player_ship.png'] }

    args.outputs.labels << [ 50, 680, "HI SCORE: #{args.state.hi_score}", 3, 0, 255, 255, 255 ]
  when :game_over
    args.outputs.solids << [0, 0, 1280, 720, 0, 0, 0]
    args.outputs.solids << args.state.stars.map do |star|
        {
            x: star.x,
            y: star.y, 
            w: 4,
            h: 4, 
            r: star.sparkly ? rand(255) : 255,
            g: star.sparkly ? rand(255) : 255,
            b: star.sparkly ? rand(255) : 255,
            a: 80 * (args.state.tick_count % star.twinkle)
        }
    end

    args.outputs.sprites << args.state.explosions.map do |ex|
        {
            x: ex.x - (ex.size / 2),
            y: ex.y - (ex.size / 2),
            w: ex.size,
            h: ex.size,
            path: 'sprites/explosion.png',
            tile_x: ex.sprite_animation.frames[ex.current_animation_frame].top_left_x,
            tile_y: ex.sprite_animation.frames[ex.current_animation_frame].top_left_y,
            tile_w: ex.sprite_animation.w,
            tile_h: ex.sprite_animation.h,
            a: 240,
            angle: ex.angle
        }
    end

    args.outputs.labels << [ 640, 500, 'GAME OVER', 16, 1, 255, 25, 25 ]
    hi = args.state.player_score == args.state.hi_score
    args.outputs.labels << [ 640, 400, "YOUR SCORE: #{args.state.player_score}", 4, 1, hi ? rand(255) : 255, hi ? rand(255) : 255, hi ? rand(255) : 255]
    args.outputs.labels << [ 640, 350, "HI SCORE: #{args.state.hi_score}", 4, 1, hi ? rand(255) : 255, hi ? rand(255) : 255, hi ? rand(255) : 255]

  else
    args.outputs.solids << [0, 0, 1280, 720, 255, 0, 0]
    args.outputs.labels << [ 640, 500, 'GURU MEDITATION', 16, 1, 0, 0, 0 ]
  end
end

def init_starfield args
  args.state.stars = []
  args.state.number_of_stars.times do
    args.state.stars << args.state.new_entity(:star) do |s|
      s.x = rand(1280)
      s.y = rand(720)
      s.fall_speed = 1 + rand(4)
      s.twinkle = 1 + rand(4)
      s.sparkly = rand(20) == 1
    end
  end
end

def init_title_tiles args
  args.state.title_tiles = []
  20.times do |i|
    args.state.title_tiles << []
    12.times do |j|
      args.state.title_tiles[i] << rand(32)
    end
  end
end

def update_title_tiles args
  20.times do |i|
    12.times do |j|
      if i == 0 || i == 19 || j == 0 || j == 11
        args.state.title_tiles[i][j] = rand(42) if rand(16) == 0
      else
        args.state.title_tiles[i][j] = calculate_life(args, i, j)
      end
    end
  end
end

def calculate_life args, i, j
  return 0 if args.state.title_tiles[i][j] >= 80
  value = args.state.title_tiles[i - 1][j - 1] + args.state.title_tiles[i][j - 1] + args.state.title_tiles[i + 1][j - 1] +
    args.state.title_tiles[i - 1][j] + args.state.title_tiles[i + 1][j] +
    args.state.title_tiles[i - 1][j + 1] + args.state.title_tiles[i][j + 1] + args.state.title_tiles[i + 1][j + 1]
  if value < 40
    0
  elsif value < 120
    args.state.title_tiles[i][j] + 4
  else
    args.state.title_tiles[i][j] - 5
  end
end

def update_starfield args
  # Make each star fall
  args.state.stars.each do |star|
    star.y -= star.fall_speed
  end

  args.state.stars.map! do |star|
    star.y > 0 ? star : nil
  end

  args.state.stars.compact!

  # Add new stars 
  if args.state.stars.length < args.state.number_of_stars
    (args.state.number_of_stars - args.state.stars.length).times do
      args.state.stars << args.state.new_entity(:star) do |s|
        s.x = rand(1280)
        s.y = 740
        s.fall_speed = 1 + rand(4)
        s.twinkle = 1 + rand(4)
        s.sparkly = rand(20) == 1
      end
    end
  end
end

def update_player_bullets args
  if args.state.player_bullet_volley_cooldown_current > 0
    args.state.player_bullet_volley_cooldown_current -= 1
    if args.state.player_bullet_volley_cooldown_current == 0
      args.state.player_current_bullet_volley_rounds = 0
    end
  end

  args.state.player_fire_cooldown_current -= 1 if args.state.player_fire_cooldown_current > 0

  args.state.player_bullets.each { |b| b.y += b.speed }
  args.state.player_bullets.map! do |b| 
    if b.spent
      nil
    else
      b.y < 800 ? b : nil 
    end
  end
  args.state.player_bullets.compact!
end

def update_blaster_bullets args
  args.state.blaster_bullets.each { |b| b.y -= b.speed }
  args.state.blaster_bullets.map! do |b| 
    b.y > -40 ? b : nil 
  end
  args.state.blaster_bullets.compact!
end

def update_mine_explosions args
  args.state.mine_explosions.each { |x| x.age += 1 }
  args.state.mine_explosions.map! { |x| x.age < x.duration ? x : nil }
  args.state.mine_explosions.compact!
end

def update_heavy_missiles args
  args.state.heavy_missiles.each { |m| m.update args }
  args.state.heavy_missiles.map! { |m| m.alive && m.x > -100 && m.x < 1400 && m.y > -100 && m.y < 820 ? m : nil }
  args.state.heavy_missiles.compact!
end

def update_enemies args
  args.state.blasters_fired_this_tick = false

  args.state.enemies.each { |e| e.update args }

  args.outputs.sounds << "sounds/blaster_fire.wav" if args.state.blasters_fired_this_tick

  args.state.enemies.map! do |e|
    if e.alive
      e.y > -10 ? e : nil
    else
      nil
    end
  end

  args.state.enemies.compact!

  args.state.next_enemy_ticker += 1
  if args.state.next_enemy_ticker >= args.state.enemy_spawn_period
    spawn_enemy args
    args.state.next_enemy_ticker = 0
  end
end

def spawn_enemy args
  # TODO: Avoid having a magic number for number of enemy types
  # TODO: Introduce enemies over time, have easier enemies more common

  new_enemy_type = 0
  new_enemy_number = 1
  case args.state.current_stage
  when 0
    new_enemy_type = 0
  when 1
    new_enemy_type = rand(2)
  when 2
    new_enemy_type = rand(4)
    if [0,1,2].include? new_enemy_type
      new_enemy_number = 1 + rand(2)
    end
  else
    new_enemy_type = rand(6)
    new_enemy_number = 1 + rand(args.state.current_stage)
  end

  new_enemy_sym = case new_enemy_type
  when 0 # bug
    :bug
  when 1 # wasp
    :wasp
  when 2 # mine
    :mine
  when 3 # blaster
    :blaster
  when 4 # amoeba
    :amoeba
  when 5 # heavy
    :heavy
  else
    :bug
  end

  new_enemy_number.times { args.state.enemies << Enemy.new(new_enemy_sym, rand(1200) + 40, 760) }
end

def update_explosions args
  args.state.explosions.each { |ex| ex.update }

  args.state.explosions.map! { |ex| ex.finished? ? nil : ex }
  args.state.explosions.compact!
end

def update_stage args
  case args.state.current_stage
  when 0
    args.state.current_stage = 1 if args.state.player_score > 10
  when 1
    args.state.current_stage = 2 if args.state.player_score > 50
  when 2
    args.state.current_stage = 3 if args.state.player_score > 110
  else
    # Every 500 starting at 250
    args.state.current_stage += 1 if args.state.player_score > 500 * args.state.current_stage - 1250
  end
end

def process_collisions args
  # between player bullets and enemies
  args.state.player_bullets.each_with_index do |b, i|
    next if b.spent
    args.state.enemies.each do |e|
      next unless e.alive
      if e.collision_rect.intersect_rect?({ x: b.x - 2, y: b.y + 8, w: 4, h: 16 })
        # TODO: handle explosion, score increment etc if apply_damage kills the enemy
        if e.apply_damage(args.state.player_bullet_damage)
          if e.type == :mine
            args.outputs.sounds << "sounds/mine_explosion.wav"
            add_explosions(args, e.x, e.y, 256)
            args.state.mine_explosions << args.state.new_entity(:mine_explosion) do |mx|
              mx.x = e.x
              mx.y = e.y
              mx.radius = 128
              mx.age = 0
              mx.duration = 4
              mx.damage = 4
            end
          else
            args.outputs.sounds << "sounds/explosion.wav"
            add_explosions(args, e.x, e.y)
          end
          args.state.player_score += e.points + args.state.current_stage
        end
        b.spent = true
      end
      break if b.spent
    end
  end

  # Between mine_explosions and enemies
  args.state.mine_explosions.each do |mx|
    args.state.enemies.each do |e|
      next unless e.alive
      if { x: mx.x - mx.radius, y: mx.y - mx.radius, w: mx.radius * 2, h: mx.radius * 2 }.intersect_rect?(e.collision_rect)
        if e.apply_damage(mx.damage)
          if e.type == :mine
            args.outputs.sounds << "sounds/mine_explosion.wav"
            add_explosions(args, e.x, e.y, 256)
            args.state.mine_explosions << args.state.new_entity(:mine_explosion) do |mx|
              mx.x = e.x
              mx.y = e.y
              mx.radius = 128
              mx.age = 0
              mx.duration = 16
              mx.damage = 1
            end
          else
            args.outputs.sounds << "sounds/explosion.wav"
            add_explosions(args, e.x, e.y)
          end
          args.state.player_score += e.points + 5 * args.state.current_stage
        end
      end
    end
  end

  unless !args.state.player_currently_alive || player_in_iframes?(args)
    # Between enemies and player
    args.state.enemies.each do |e|
      break unless args.state.player_currently_alive
      if e.collision_rect.intersect_rect?({ x: args.state.current_player_x - 16, y: args.state.current_player_y - 16, w: 32, h: 32 })
        args.state.player_currently_alive = false
        args.outputs.sounds << "sounds/explosion.wav"
        add_explosions(args, args.state.current_player_x, args.state.current_player_y)
      end
    end

    # between blaster bullets and player
    args.state.blaster_bullets.each do |bb|
      break unless args.state.player_currently_alive
      if { x: bb.x - 2, y: bb.y - 4, w: 4, h: 8 }.intersect_rect?({ x: args.state.current_player_x - 16, y: args.state.current_player_y - 16, w: 32, h: 32 })
        args.state.player_currently_alive = false
        args.outputs.sounds << "sounds/explosion.wav"
        add_explosions(args, args.state.current_player_x, args.state.current_player_y)
      end
    end

    # between mine explosions and player
    args.state.mine_explosions.each do |mx|
      break unless args.state.player_currently_alive
      if { x: mx.x - mx.radius, y: mx.y - mx.radius, w: mx.radius * 2, h: mx.radius * 2 }.intersect_rect?({ x: args.state.current_player_x - 16, y: args.state.current_player_y - 16, w: 32, h: 32 })
        args.state.player_currently_alive = false
        args.outputs.sounds << "sounds/explosion.wav"
        add_explosions(args, args.state.current_player_x, args.state.current_player_y)
      end
    end

    # Between heavy missiles and player
    args.state.heavy_missiles.each do |m|
      break unless args.state.player_currently_alive
      if m.alive && { x: m.x - 4, y: m.y - 4, w: 8, h: 8}.intersect_rect?({ x: args.state.current_player_x - 16, y: args.state.current_player_y - 16, w: 32, h: 32 })
        args.state.player_currently_alive = false
        args.outputs.sounds << "sounds/explosion.wav"
        add_explosions(args, args.state.current_player_x, args.state.current_player_y)
        m.alive = false
      end
    end
  end
end

def player_in_iframes? args
  args.state.player_iframes_counter < args.state.player_iframes_duration
end

def transition_between args, from_state, to_state
  args.state.current_state = to_state
  case to_state
  when :title_screen
    args.gtk.stop_music
    args.outputs.sounds << 'music/title.ogg'
  when :playing
    #args.gtk.stop_music
    args.outputs.sounds << 'music/playing.ogg'
    args.state.enemies = []
    args.state.explosions = []
    args.state.mine_explosions = []
    args.state.blaster_bullets = []
    args.state.player_bullets = []
    args.state.heavy_missiles = []
    args.state.current_player_x = args.state.initial_player_x
    args.state.current_player_y = args.state.initial_player_y
    args.state.player_currently_alive = true
    args.state.player_lives = 3
    args.state.player_score = 0
    args.state.current_stage = 0
    args.state.cd_animation_tick = 0
  when :game_over
    args.gtk.stop_music
    if args.state.player_score >= args.state.hi_score 
      args.outputs.sounds << 'music/hi_score.ogg'
    else
      args.outputs.sounds << 'music/game_over.ogg'
    end
  end
end

def add_explosions(args, x, y, size = 128, number = 3)
  spread = size / 2
  number.times do |i|
    args.state.explosions << Explosion.new(x - spread / 2 + rand(spread), y - spread / 2 + rand(spread), size)
  end
end

def process_input args
  case args.state.current_state
  when :title_screen
    if args.inputs.controller_one.key_down.start || args.inputs.keyboard.key_down.space
      transition_between args, :title_screen, :playing
    end
  when :playing
    if args.state.player_currently_alive
      if args.inputs.controller_one.left_analog_x_perc < -0.2 || args.inputs.keyboard.key_held.left
        if args.state.current_player_x > 32
          args.state.current_player_x = (args.state.current_player_x - args.state.player_speed).clamp(32,1248)
          args.state.stars.each { |s| s.x += s.fall_speed }
        end
      end
      if args.inputs.controller_one.left_analog_x_perc > 0.2 || args.inputs.keyboard.key_held.right
        if args.state.current_player_x < 1248
          args.state.current_player_x = (args.state.current_player_x + args.state.player_speed).clamp(32,1248)
          args.state.stars.each { |s| s.x -= s.fall_speed }
        end
      end
      if (args.inputs.controller_one.key_held.a || args.inputs.keyboard.key_held.space) && args.state.player_bullet_volley_cooldown_current == 0 && args.state.player_fire_cooldown_current == 0
        args.state.player_bullets << PlayerBullet.new(args.state.current_player_x, args.state.current_player_y + 48, args.state.player_bullet_speed, false)
        args.outputs.sounds << "sounds/player_fire.wav"
        args.state.player_fire_cooldown_current = args.state.player_fire_cooldown_duration
        args.state.player_current_bullet_volley_rounds += 1
        if args.state.player_current_bullet_volley_rounds >= args.state.player_bullet_volley_size
          args.state.player_bullet_volley_cooldown_current = args.state.player_bullet_volley_cooldown_duration
        end
      end
    end
  when :game_over
    if args.inputs.controller_one.key_down.start || args.inputs.keyboard.key_down.space
      transition_between args, :game_over, :title_screen
    end
  end
end

def update args
  case args.state.current_state
  when :title_screen
    if args.state.tick_count % 4 == 0
      update_title_tiles args
      args.state.title_marquee_right_char += 1
      #args.state.title_marquee_left_char += 1 if args.state.title_marquee_right_char >= args.state.title_marquee_char_width
      if args.state.title_marquee_right_char >= args.state.title_marquee_text.length
        args.state.title_marquee_left_char = 0
        args.state.title_marquee_right_char = 1
      end
    end
  when :playing
    update_starfield args
    update_stage args
    update_player_bullets args
    update_blaster_bullets args
    update_mine_explosions args
    update_heavy_missiles args
    update_enemies args
    update_explosions args
    process_collisions args

    args.state.cd_animation_tick += 1 if args.state.cd_animation_tick < args.state.cd_animation_max

    if args.state.player_currently_alive
      args.state.player_iframes_counter = [args.state.player_iframes_counter + 1, args.state.player_iframes_duration].min
    else
      if args.state.player_lives == 0
        transition_between args, :playing, :game_over
      else
        args.state.player_respawn_current += 1
        if args.state.player_respawn_current >= args.state.player_respawn_duration
          args.state.player_respawn_current = 0
          args.state.player_lives -= 1
          args.state.player_currently_alive = true
          args.state.current_player_x = 640.0
          args.state.player_iframes_counter = 0
          args.state.current_stage = 0
        end
      end
    end

    args.state.hi_score = [args.state.player_score, args.state.hi_score].max
  when :game_over
    update_starfield args
    update_explosions args
  end
end

def tick args
  if args.state.tick_count == 0
    args.gtk.stop_music
    args.audio[:test] = { input: 'music/title.ogg', gain: 0.0 }
    init_title_tiles args
  end
  if args.state.tick_count == 1
    args.audio[:test] = { input: 'music/playing.ogg', gain: 0.0 }
  end
  if args.state.tick_count == 2
    args.outputs.sounds << 'music/title.ogg'
  end
  defaults args
  process_input args
  update args
  render args
end