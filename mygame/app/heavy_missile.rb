require 'app/sprite_animation.rb'

class HeavyMissile
  attr_reader :x, :y, :angle, :sprite_animation, :current_animation_frame, :speed
  attr_accessor :alive

  def initialize(x, y, angle)
    @x = x
    @y = y
    @angle = angle
    @alive = true
    @charging = false
    @sprite_animation = @@sprite_animation_heavy_missile
    @current_animation_frame = 0
    @speed = 2
  end

  def update args
    if @charging
        @speed = @speed * 1.05
    else
      # Aim slightly high :)
      angle_to_player = Math.atan2(args.state.current_player_y - @y + 10, args.state.current_player_x - @x) * 180 / Math::PI + 90
      if (@angle - angle_to_player).abs > 4
        @angle -= 3 if @angle - angle_to_player > 4.0
        @angle += 3 if @angle - angle_to_player < -4.0
      else
        @charging = true
      end
    end
    @x += @speed * Math.sin(@angle * Math::PI / 180)
    @y -= @speed * Math.cos(@angle * Math::PI / 180)
  end

  def serialize
    {}
  end

  def inspect
    serialize.to_s
  end

  def to_s
    serialize.to_s
  end
end