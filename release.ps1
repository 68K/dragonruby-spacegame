# Fails to download butler, so only package
.\dragonruby-publish.exe --only-package

Compress-Archive -Update .\builds\spacegame-linux-amd64.bin .\builds\spacegame-linux-amd64.zip
Compress-Archive -Update .\builds\spacegame-linux-raspberrypi.bin .\builds\spacegame-linux-raspberrypi.zip
Compress-Archive -Update .\builds\spacegame-windows-amd64.exe .\builds\spacegame-windows-amd64.zip

./butler push .\builds\spacegame-linux-amd64.zip 68ST0X20/space-game:linux
./butler push .\builds\spacegame-linux-raspberrypi.zip 68ST0X20/space-game:raspberrypi
./butler push .\builds\spacegame-windows-amd64.zip 68ST0X20/space-game:windows
./butler push .\builds\spacegame-macos.zip 68ST0X20/space-game:macos